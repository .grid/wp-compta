<?php

include( untrailingslashit(TEMPLATEPATH) . '/inc/cron.php' );
include( untrailingslashit(TEMPLATEPATH) . '/inc/theme-helpers.php' );
include( untrailingslashit(TEMPLATEPATH) . '/inc/post-types.php' );
include( untrailingslashit(TEMPLATEPATH) . '/inc/taxonomies.php' );
include( untrailingslashit(TEMPLATEPATH) . '/inc/acf-invoice.php' );

function wpcompta_enqueue_scripts() {
    wp_enqueue_style( 'wp-compta', get_stylesheet_uri() );
    wp_enqueue_style( 'wp-compta-print', get_template_directory_uri() . '/print.css', 'wp-compta', null, 'print' );
    wp_enqueue_script( 'script-name', get_template_directory_uri() . '/js/scripts.js', array(), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'wpcompta_enqueue_scripts' );

//wp_register_script('admin_title_disable', get_template_directory_uri() . '/assets/js/admin.js');
function disableAdminTitle () {
  //wp_enqueue_script('admin_title_disable');
}
//add_action('admin_enqueue_scripts', 'disableAdminTitle');

// Tools Compta

function get_current_invoiceID(){

    // Get all published Invoices
    $arrayInvoices = array(
        'post_type' => 'invoice',
        'status' => 'publish',
        'posts_per_page' => -1
    );

    $queryInvoices = new WP_Query( $arrayInvoices );

    $countInvoices = $queryInvoices->found_posts;

    wp_reset_query();

    return sprintf('%03d', $countInvoices);

}

function get_tax_value($amount){

    $tax = floatval($amount) * 0.2;

    return $tax;

}

if (is_admin()){
    //add_filter('default_title', 'wpshout_filter_example');
    function wpshout_filter_example($title) {
        $title = 'F'. date('Ymd') . '-' . get_current_invoiceID();
        return $title;
    }
}

add_filter('acf/settings/save_json', 'my_acf_json_save_point');

function my_acf_json_save_point( $path ) {

    // update path
    $path = get_stylesheet_directory() . '/acf';


    // return
    return $path;

}

add_filter('acf/settings/load_json', 'my_acf_json_load_point');

function my_acf_json_load_point( $paths ) {

    // remove original path (optional)
    unset($paths[0]);

    // append path
    $paths[] = get_stylesheet_directory() . '/acf';

    // return
    return $paths;

}

// Add Readonly option to Input[type="text"]
add_action('acf/render_field_settings/type=text', 'add_readonly_and_disabled_to_text_field');
  function add_readonly_and_disabled_to_text_field($field) {
    acf_render_field_setting( $field, array(
      'label'      => __('Read Only?','acf'),
      'instructions'  => '',
      'type'      => 'radio',
      'name'      => 'readonly',
      'choices'    => array(
        1        => __("Yes",'acf'),
        0        => __("No",'acf'),
      ),
      'layout'  =>  'horizontal',
    ));
    acf_render_field_setting( $field, array(
      'label'      => __('Disabled?','acf'),
      'instructions'  => '',
      'type'      => 'radio',
      'name'      => 'disabled',
      'choices'    => array(
        1        => __("Yes",'acf'),
        0        => __("No",'acf'),
      ),
      'layout'  =>  'horizontal',
    ));
  }

// Update Readonly field saving Amount
add_action('acf/save_post', 'save_amount');
function save_amount( $post_id ) {

    // Get newly saved values.
    $values = get_fields( $post_id );
    $invoiceTotalPrice = 0;

    // Check the new value of a specific field.
    while( have_rows('invoice_services', $post_id) ): the_row();
        $service = get_sub_field('invoice_service_group', $post_id);
        $serviceTitle = $service['invoice_service_title'];
        $descriptionExtras = $service['invoice_service_details'];
        $quantity = floatval(get_sub_field('invoice_service_quantity', $post_id)) > 0 ? floatval(get_sub_field('invoice_service_quantity', $post_id)) : '-';
        //$tax = '20%';
        $priceUnit = intval(get_sub_field('invoice_service_unit_price', $post_id));
        $priceTotal = isset($quantity) && $quantity > 0 ? $quantity * $priceUnit : $priceUnit;

        $invoiceTotalPrice += floatval($priceTotal);
    endwhile;
    update_field('invoice_amount', $invoiceTotalPrice);
}

add_filter( 'display_post_states', 'custom_state_client', 10, 2 );
function custom_state_client($post_states, $post){
    if( get_post_type($post) === 'quote' || get_post_type($post) === 'invoice' ){
        $clientID = get_field('invoice_client_id', $post->ID);
        $clientName = get_the_title($clientID);
        $post_states[] = $clientName;
    }
    return $post_states;
}