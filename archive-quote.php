<?php 
get_header();

    $objects = get_queried_object();
    $postType = $objects->name;
    //var_dump($objects);

    // Display all Quotes
    $argsQuotes = array(
        'post_type' => $postType,
        'posts_per_page' => -1
    );

    $queryQuotes = new WP_Query( $argsQuotes );

    if( $queryQuotes->have_posts() ) :
        ?>
        <table>
            <thead>
                <tr>
                    <th>Devis</th>
                    <th>Client</th>
                </tr>
            </thead>
        <?php
        while( $queryQuotes->have_posts() ) : $queryQuotes->the_post();

            /**
             * Init data
             */
            $id = get_the_ID();
            $name = get_the_title();
            
            $clientID = get_field('invoice_client_id', $id);
            $clientName = get_the_title($clientID);


        ?>
        <tr>
        <?php
            echo '<td>' . get_the_title() . '</td>';
            echo '<td>' . $clientName . '</td>';
        ?>
        </tr>
        <?php
        endwhile;
        wp_reset_postdata();
        ?>
        </table>
        <?php
    endif;


get_footer();