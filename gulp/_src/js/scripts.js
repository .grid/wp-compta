import jQuery from 'jquery'
window.$ = window.jQuery = jQuery;

require( 'datatables.net' )( window, $ );

$(function(){

    var mainHandler = {
        
        init: function(){
            jQuery('#table-invoices').DataTable({
                paging: false,
            });

        }

    };

    mainHandler.init();

});