<?php
/**
 * The template for displaying single posts and pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

get_header();

	if ( have_posts() ) {

		while ( have_posts() ) {
            the_post();

            get_template_part('template-parts/invoice/single-invoice');
            
		}
	}

	?>

<?php get_footer();