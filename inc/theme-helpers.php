<?php

/**
 * Get Status
 */
function wpc_get_status( $postID ){

    $postType = get_post_type( $postID );
    $statusField = $postType . '_status';

    // Set default empty status
    $status = '';

    if( !empty( get_field( $statusField, $postID ) ) ){
        $status = get_field( $statusField, $postID );
    }

    return $status;

}

/**
 * Get Status Tag
 */
function wpc_get_status_tag( $status ){

    $tag = '';
    $tag .= '<span class="tag-status ' . $status['value'] . '">';
    $tag .= $status['label'];
    $tag .= '</span>';

    return $tag;

}

/**
 * Get 'Late Payment' days
 */
function wpc_get_late_payment_days( $postID ){

    $invoiceSentDate = !empty( get_field('invoice_sent_date', $postID) ) ? get_field('invoice_sent_date', $postID) : '';
    $invoiceSentDate = date_create($invoiceSentDate);
    $currentDay = date('Y-m-d');
    $currentDay = date_create($currentDay);

    //difference between two dates
    $dateDiff = date_diff($invoiceSentDate,$currentDay);
    $daysDiff = $dateDiff->format("%a");

    //count days
    return $daysDiff;

}

/**
 * Get 'Late Payment' days
 */
function wpc_invoice_is_late( $postID ){
    $invoiceIsLate = wpc_get_late_payment_days( $postID ) > '60' ? true : false;
    return $invoiceIsLate;
}