<?php

add_filter( 'cron_schedules', 'wpc_cron_invoices_interval' );
function wpc_cron_invoices_interval( $schedules ) { 
    $schedules['twenty_seconds'] = array(
        'interval' => 20,
        'display'  => esc_html__( 'Every 20 Seconds' ), );
    return $schedules;
}

function wpc_update_late_invoices(){
    
    $invoices = get_posts( array(
        'post_type' => 'invoice',
        'posts_per_page' => -1
    ) );

    if ( $invoices ) {
        foreach ( $invoices as $invoice ) {
            setup_postdata($invoice);
            $invoiceID = $invoice->ID;
            $currentStatus = get_field('invoice_status', $invoiceID);
            if( wpc_invoice_is_late($invoiceID) && $currentStatus['value'] === 'sent' ){
                $statusLate = array(
                    'value' => 'late'
                );
                update_field('invoice_status', $statusLate, $invoiceID);
            }
        }
    }
}

add_action( 'wpc_invoices_late', 'wpc_update_late_invoices' );
if ( ! wp_next_scheduled( 'wpc_invoices_late' ) ) {
    wp_schedule_event( time(), 'twenty_seconds', 'wpc_invoices_late' );
}