<?php

// Register Custom Taxonomy
function register_quote_status() {

	$labels = array(
		'name'                       => _x( 'Statuses', 'Taxonomy General Name', 'wp-compta' ),
		'singular_name'              => _x( 'Quote Status', 'Taxonomy Singular Name', 'wp-compta' ),
		'menu_name'                  => __( 'Statuses', 'wp-compta' ),
		'all_items'                  => __( 'All Items', 'wp-compta' ),
		'parent_item'                => __( 'Parent Item', 'wp-compta' ),
		'parent_item_colon'          => __( 'Parent Item:', 'wp-compta' ),
		'new_item_name'              => __( 'New Item Name', 'wp-compta' ),
		'add_new_item'               => __( 'Add New Item', 'wp-compta' ),
		'edit_item'                  => __( 'Edit Item', 'wp-compta' ),
		'update_item'                => __( 'Update Item', 'wp-compta' ),
		'view_item'                  => __( 'View Item', 'wp-compta' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'wp-compta' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'wp-compta' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'wp-compta' ),
		'popular_items'              => __( 'Popular Items', 'wp-compta' ),
		'search_items'               => __( 'Search Items', 'wp-compta' ),
		'not_found'                  => __( 'Not Found', 'wp-compta' ),
		'no_terms'                   => __( 'No items', 'wp-compta' ),
		'items_list'                 => __( 'Items list', 'wp-compta' ),
		'items_list_navigation'      => __( 'Items list navigation', 'wp-compta' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'quote_status', array( 'quote', 'invoice' ), $args );

}
add_action( 'init', 'register_quote_status', 0 );

// Register Custom Taxonomy
function register_invoice_status() {

	$labels = array(
		'name'                       => _x( 'Statuses', 'Taxonomy General Name', 'wp-compta' ),
		'singular_name'              => _x( 'Invoice Status', 'Taxonomy Singular Name', 'wp-compta' ),
		'menu_name'                  => __( 'Statuses', 'wp-compta' ),
		'all_items'                  => __( 'All Items', 'wp-compta' ),
		'parent_item'                => __( 'Parent Item', 'wp-compta' ),
		'parent_item_colon'          => __( 'Parent Item:', 'wp-compta' ),
		'new_item_name'              => __( 'New Item Name', 'wp-compta' ),
		'add_new_item'               => __( 'Add New Item', 'wp-compta' ),
		'edit_item'                  => __( 'Edit Item', 'wp-compta' ),
		'update_item'                => __( 'Update Item', 'wp-compta' ),
		'view_item'                  => __( 'View Item', 'wp-compta' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'wp-compta' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'wp-compta' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'wp-compta' ),
		'popular_items'              => __( 'Popular Items', 'wp-compta' ),
		'search_items'               => __( 'Search Items', 'wp-compta' ),
		'not_found'                  => __( 'Not Found', 'wp-compta' ),
		'no_terms'                   => __( 'No items', 'wp-compta' ),
		'items_list'                 => __( 'Items list', 'wp-compta' ),
		'items_list_navigation'      => __( 'Items list navigation', 'wp-compta' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'invoice_status', array( 'invoice' ), $args );

}
//add_action( 'init', 'register_invoice_status', 0 );