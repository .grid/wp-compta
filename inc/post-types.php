<?php

// Register Quotes
function register_quote() {

	$labels = array(
		'name'                  => _x( 'Quotes', 'Post Type General Name', 'wp-compta' ),
		'singular_name'         => _x( 'Quote', 'Post Type Singular Name', 'wp-compta' ),
		'menu_name'             => __( 'Quotes', 'wp-compta' ),
		'name_admin_bar'        => __( 'Quotes', 'wp-compta' ),
		'archives'              => __( 'Quotes', 'wp-compta' ),
		'attributes'            => __( 'Quote Attributes', 'wp-compta' ),
		'parent_item_colon'     => __( 'Parent Quote:', 'wp-compta' ),
		'all_items'             => __( 'All Quotes', 'wp-compta' ),
		'add_new_item'          => __( 'Add New Quote', 'wp-compta' ),
		'add_new'               => __( 'Add New', 'wp-compta' ),
		'new_item'              => __( 'New Quote', 'wp-compta' ),
		'edit_item'             => __( 'Edit Quote', 'wp-compta' ),
		'update_item'           => __( 'Update Quote', 'wp-compta' ),
		'view_item'             => __( 'View Quote', 'wp-compta' ),
		'view_items'            => __( 'View Quotes', 'wp-compta' ),
		'search_items'          => __( 'Search Quote', 'wp-compta' ),
		'not_found'             => __( 'Not found', 'wp-compta' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'wp-compta' ),
		'featured_image'        => __( 'Logo', 'wp-compta' ),
		'set_featured_image'    => __( 'Set logo', 'wp-compta' ),
		'remove_featured_image' => __( 'Remove logo', 'wp-compta' ),
		'use_featured_image'    => __( 'Use as logo', 'wp-compta' ),
		'insert_into_item'      => __( 'Insert into Quote', 'wp-compta' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Quote', 'wp-compta' ),
		'items_list'            => __( 'Quotes list', 'wp-compta' ),
		'items_list_navigation' => __( 'Quotes list navigation', 'wp-compta' ),
		'filter_items_list'     => __( 'Filter Quotes list', 'wp-compta' ),
	);
	$args = array(
		'label'                 => __( 'quote', 'wp-compta' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9Ijc2OCIgaGVpZ2h0PSI3NjgiIHZpZXdCb3g9IjAgMCA3NjggNzY4Ij48dGl0bGU+Y3VycmVuY3ktZG9sbGFyPC90aXRsZT48ZyBpZD0iaWNvbW9vbi1pZ25vcmUiPjwvZz48cGF0aCBmaWxsPSIjZmZmZmZmIiBkPSJNMzE5LjUgNTQ0LjVsMjU2LjUtMjU2LjUtNDUtNDUtMjExLjUgMjEwLTgyLjUtODIuNS00NSA0NXpNMzg0IDk2cS0xMy41IDAtMjIuNSA5dC05IDIyLjUgOSAyMy4yNSAyMi41IDkuNzUgMjIuNS05Ljc1IDktMjMuMjUtOS0yMi41LTIyLjUtOXpNNjA3LjUgOTZxMjUuNSAwIDQ1IDE5LjV0MTkuNSA0NXY0NDdxMCAyNS41LTE5LjUgNDV0LTQ1IDE5LjVoLTQ0N3EtMjUuNSAwLTQ1LTE5LjV0LTE5LjUtNDV2LTQ0N3EwLTI1LjUgMTkuNS00NXQ0NS0xOS41aDEzMy41cTEwLjUtMjguNSAzNC41LTQ2LjV0NTUuNS0xOCA1NS41IDE4IDM0LjUgNDYuNWgxMzMuNXoiPjwvcGF0aD48L3N2Zz4=',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'rewrite'				=> array(
			'slug'		=> 'liste-devis',
		)
	);
	register_post_type( 'quote', $args );

}
add_action( 'init', 'register_quote', 0 );

// Register Invoices
function register_invoice() {

	$labels = array(
		'name'                  => _x( 'Invoices', 'Post Type General Name', 'wp-compta' ),
		'singular_name'         => _x( 'Invoice', 'Post Type Singular Name', 'wp-compta' ),
		'menu_name'             => __( 'Invoices', 'wp-compta' ),
		'name_admin_bar'        => __( 'Invoices', 'wp-compta' ),
		'archives'              => __( 'Invoices', 'wp-compta' ),
		'attributes'            => __( 'Invoice Attributes', 'wp-compta' ),
		'parent_item_colon'     => __( 'Parent Invoice:', 'wp-compta' ),
		'all_items'             => __( 'All Invoices', 'wp-compta' ),
		'add_new_item'          => __( 'Add New Invoice', 'wp-compta' ),
		'add_new'               => __( 'Add New', 'wp-compta' ),
		'new_item'              => __( 'New Invoice', 'wp-compta' ),
		'edit_item'             => __( 'Edit Invoice', 'wp-compta' ),
		'update_item'           => __( 'Update Invoice', 'wp-compta' ),
		'view_item'             => __( 'View Invoice', 'wp-compta' ),
		'view_items'            => __( 'View Invoices', 'wp-compta' ),
		'search_items'          => __( 'Search Invoice', 'wp-compta' ),
		'not_found'             => __( 'Not found', 'wp-compta' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'wp-compta' ),
		'featured_image'        => __( 'Logo', 'wp-compta' ),
		'set_featured_image'    => __( 'Set logo', 'wp-compta' ),
		'remove_featured_image' => __( 'Remove logo', 'wp-compta' ),
		'use_featured_image'    => __( 'Use as logo', 'wp-compta' ),
		'insert_into_item'      => __( 'Insert into Invoice', 'wp-compta' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Invoice', 'wp-compta' ),
		'items_list'            => __( 'Invoices list', 'wp-compta' ),
		'items_list_navigation' => __( 'Invoices list navigation', 'wp-compta' ),
		'filter_items_list'     => __( 'Filter Invoices list', 'wp-compta' ),
	);
	$rewrite = array(
		'slug'		=> 'factures',
	);
	$args = array(
		'label'                 => __( 'Invoice', 'wp-compta' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9Ijc2OCIgaGVpZ2h0PSI3NjgiIHZpZXdCb3g9IjAgMCA3NjggNzY4Ij48dGl0bGU+Y3VycmVuY3ktZG9sbGFyPC90aXRsZT48ZyBpZD0iaWNvbW9vbi1pZ25vcmUiPjwvZz48cGF0aCBmaWxsPSIjZmZmZmZmIiBkPSJNMzc4IDM0OS41cTMzIDkgNTQuNzUgMTh0NDUuNzUgMjQuNzUgMzYuNzUgMzkuNzUgMTIuNzUgNTdxMCA0Ni41LTMwLjc1IDc1Ljc1dC04MS43NSAzOC4yNXY2OWgtOTZ2LTY5cS00OS41LTEwLjUtODEuNzUtNDJ0LTM1LjI1LTgxaDcwLjVxNiA2Ny41IDk0LjUgNjcuNSA0Ni41IDAgNjYuNzUtMTcuMjV0MjAuMjUtMzkuNzVxMC01NC05Ni03OC0xNTAtMzQuNS0xNTAtMTMyIDAtNDMuNSAzMS41LTc0LjI1dDc5LjUtNDEuMjV2LTY5aDk2djcwLjVxNDkuNSAxMiA3NS43NSA0NXQyNy43NSA3Ni41aC03MC41cS0zLTY3LjUtODEtNjcuNS0zOSAwLTYyLjI1IDE2LjV0LTIzLjI1IDQzLjVxMCA0My41IDk2IDY5eiI+PC9wYXRoPjwvc3ZnPg==',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'rewrite'				=> $rewrite
	);
	register_post_type( 'invoice', $args );

}
add_action( 'init', 'register_invoice', 0 );

// Register Clients
function register_client() {

	$labels = array(
		'name'                  => _x( 'Clients', 'Post Type General Name', 'wp-compta' ),
		'singular_name'         => _x( 'Client', 'Post Type Singular Name', 'wp-compta' ),
		'menu_name'             => __( 'Clients', 'wp-compta' ),
		'name_admin_bar'        => __( 'Clients', 'wp-compta' ),
		'archives'              => __( 'Clients', 'wp-compta' ),
		'attributes'            => __( 'Client Attributes', 'wp-compta' ),
		'parent_item_colon'     => __( 'Parent Client:', 'wp-compta' ),
		'all_items'             => __( 'All Clients', 'wp-compta' ),
		'add_new_item'          => __( 'Add New Client', 'wp-compta' ),
		'add_new'               => __( 'Add New', 'wp-compta' ),
		'new_item'              => __( 'New Client', 'wp-compta' ),
		'edit_item'             => __( 'Edit Client', 'wp-compta' ),
		'update_item'           => __( 'Update Client', 'wp-compta' ),
		'view_item'             => __( 'View Client', 'wp-compta' ),
		'view_items'            => __( 'View Clients', 'wp-compta' ),
		'search_items'          => __( 'Search Client', 'wp-compta' ),
		'not_found'             => __( 'Not found', 'wp-compta' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'wp-compta' ),
		'featured_image'        => __( 'Logo', 'wp-compta' ),
		'set_featured_image'    => __( 'Set logo', 'wp-compta' ),
		'remove_featured_image' => __( 'Remove logo', 'wp-compta' ),
		'use_featured_image'    => __( 'Use as logo', 'wp-compta' ),
		'insert_into_item'      => __( 'Insert into Client', 'wp-compta' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Client', 'wp-compta' ),
		'items_list'            => __( 'Clients list', 'wp-compta' ),
		'items_list_navigation' => __( 'Clients list navigation', 'wp-compta' ),
		'filter_items_list'     => __( 'Filter Clients list', 'wp-compta' ),
	);
	$args = array(
		'label'                 => __( 'Client', 'wp-compta' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9Ijc2OCIgaGVpZ2h0PSI3NjgiIHZpZXdCb3g9IjAgMCA3NjggNzY4Ij48dGl0bGU+Y3VycmVuY3ktZG9sbGFyPC90aXRsZT48ZyBpZD0iaWNvbW9vbi1pZ25vcmUiPjwvZz48cGF0aCBmaWxsPSIjZmZmZmZmIiBkPSJNNjA3LjUgNDgwdi02NC41aC02M3Y2NC41aDYzek02MDcuNSA2MDcuNXYtNjNoLTYzdjYzaDYzek00MTUuNSAyMjMuNXYtNjNoLTYzdjYzaDYzek00MTUuNSAzNTIuNXYtNjQuNWgtNjN2NjQuNWg2M3pNNDE1LjUgNDgwdi02NC41aC02M3Y2NC41aDYzek00MTUuNSA2MDcuNXYtNjNoLTYzdjYzaDYzek0yMjMuNSAzNTIuNXYtNjQuNWgtNjN2NjQuNWg2M3pNMjIzLjUgNDgwdi02NC41aC02M3Y2NC41aDYzek0yMjMuNSA2MDcuNXYtNjNoLTYzdjYzaDYzek00ODAgMzUyLjVoMTkydjMxOS41aC01NzZ2LTQ0OC41aDE5MnYtNjNsOTYtOTYgOTYgOTZ2MTkyeiI+PC9wYXRoPjwvc3ZnPg==',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'client', $args );

}
add_action( 'init', 'register_client', 0 );