<?php
require_once(untrailingslashit(TEMPLATEPATH) . '/inc/vendors/dompdf/autoload.inc.php');
use Dompdf\Dompdf;
use Dompdf\Options;

function get_invoice_status(){
	return array(
		'draft' => __('Draft', 'wp-compta'),
        'sent' => __('Sent', 'wp-compta'),
        'paid' => __('Paid', 'wp-compta'),
		'late' => __('Late', 'wp-compta')
	);
}


add_filter('acf/load_field/name=invoice_status', 'acf_load_invoice_status_choices');
function acf_load_invoice_status_choices( $field ) {
    $field['choices'] = array();
		$choices = get_invoice_status();
    if( is_array($choices) ) {
        foreach( $choices as $name => $label ) {
            $field['choices'][ $name ] = $label;
        }
    }
    return $field;
}

add_action('acf/save_post', 'wpc_save_invoice_history', 5);
function wpc_save_invoice_history( $post_id ){
    // Get previous values.
    $prev_values = get_fields( $post_id );
    $previousStatus = $prev_values['invoice_status']['value'];
    $previousLogs = !empty($prev_values['invoice_history']) && isset($prev_values['invoice_history']) ? $prev_values['invoice_history'] : '';
    //var_dump('<pre>debug:: ', $previousStatus, '</pre>');
    // Get submitted values.
    $values = $_POST['acf'];
    $nextStatus = isset($_POST['acf']['field_5f9eded33929b']) ? $_POST['acf']['field_5f9eded33929b'] : '';

    
    //die(var_dump($_POST['acf']['field_5f9eded33929b'], $previousStatus, $nextStatus));

    // Check if a specific value was updated.
    if(  !empty($nextStatus) && $nextStatus !== $previousStatus ) {
        $log = '';
        if( $previousLogs ){
            $log .= $previousLogs;
            $log .= '\n\n';
        }
        $log .= '===== ' . current_time('mysql') . ' =====\n';
        $log .= 'Status: from \'' . $previousStatus . '\' to \'' . $nextStatus . '\'';
        //$log .= '\r\n';
        //update_field('invoice_history', $log, $post_id);
        $_POST['acf']['field_5f9f2199ba684'] = stripcslashes($log);
    }
    //die(var_dump($nextStatus, $prev_values));

    if( $nextStatus !== $previousStatus && $nextStatus === 'sent' ) {
        wpc_invoice_generate_pdf($post_id);
        wpc_send_invoice($post_id, get_field('invoice_client_id', $post_id));
    }
}

function wpc_invoice_generate_pdf( $post_id ){
    error_reporting(0);
    // Get Invoice
    $domOptions = new Options();
    $domOptions->set('isRemoteEnabled', true);
    $dompdf = new Dompdf($domOptions);

    $upload_dir = wp_upload_dir();
    $invoice = get_post( $post_id );

    ob_start();
    //global $post;
    $argsInvoice = array(
        'post_type' => 'invoice',
        'p' => $post_id,
        'post_per_page' => 1
    );
    $queryInvoice = new WP_Query( $argsInvoice );
    

    if( $queryInvoice->have_posts() ) {
        while( $queryInvoice->have_posts() ) {
            $queryInvoice->the_post();
            //get_header();
            get_template_part('template-parts/invoice/pdf');
            //get_footer();
        }
        wp_reset_postdata();
    }
    $html = ob_get_clean();
    
    $dompdf->load_html($html);
    $output = $dompdf->output();
    $filename = $upload_dir['path'] . '/' . $invoice->post_title . '.pdf';
    $result = file_put_contents($filename, $output);
    $dompdf->render();
    //$dompdf->stream($invoice->post_title);
    //header('Content-type: application/pdf');
    //exit();
    //return is_int( $result );
}


function wpc_send_invoice($invoiceID, $clientID){

    // Invoice data
    $invoiceTitle = get_the_title($invoiceID);
    $upload_dir = wp_upload_dir();
    $invoiceAttachment = $upload_dir['path'] . '/' . $invoiceTitle . '.pdf';

    // Client data
    $clientName = get_field('client_firstname', $clientID) . ' ' . get_field('client_lastname');
    //$clientEmail = get_field('client_contact_email', $clientID);
    $clientEmail = 'developer@grid-agency.com';

    // Message data
    $senderEmail = 'comptabilite@grid-agency.com';
    $senderObject = 'Votre facture ' . $invoiceTitle;
    $senderMessage = '<p>Bonjour ' . $clientName . ',</p>';
    $senderMessage .= '<p>Veuillez trouver ci-joint la facture ' . $invoiceTitle . ', et mercééé <3 </p>';
    $senderMessage .= '<p>Bien cordialement,</p>';

    $headers[] = 'From: GRID <'. $senderEmail . '>';
    $headers[] = 'Cc: Rudy <rudy@grid-agency.com>';
    $headers[] = 'Content-Type: text/html; charset=UTF-8';


    wp_mail($clientEmail, $senderObject, $senderMessage, $headers, $invoiceAttachment);

}