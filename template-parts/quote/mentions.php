<?php
$customLegals = get_field('invoice_legals');
if( $customLegals ) { 
    foreach($customLegals as $customLegalsBlock ){
    ?>
    <div class="invoice--mentions-block">
        <p><strong><?php echo $customLegalsBlock['invoice_legals_title']; ?></strong></p>
        <?php if( $customLegalsBlock['invoice_legals_details_list'] ){ ?>
            <ul>
            <?php foreach( $customLegalsBlock['invoice_legals_details_list'] as $customLegalsBlockItem ){?>
                <li><?php echo $customLegalsBlockItem['invoice_legals_details']; ?></li>
            <?php } ?>
            </ul>
        <?php } ?>
    </div>
    <?php
    }

} else { ?>
<div class="invoice--mentions-block">
    <p><strong>À propos de ce devis :</strong></p>
    <ul>
        <li>Le présent devis prévoit l’intégralité des prestations que le prestataire s’engage à réaliser pour le Client.</li>
        <li>Toute prestation supplémentaire demandée par le Client donnera lieu à l'émission d’un nouveau devis ou avenant.</li>
        <li>Le présent devis est valable durant 30 jours à compter de sa date d'émission.</li>
        <li>Une fois validé par le Client, le présent devis a valeur de contrat.</li>
        <li>Dans l’hypothèse d’une rupture de contrat à l’initiative du Client, ce dernier s’engage à régler les prestations réalisées.</li>
        <li>En cas d’acceptation du devis puis de dédit, complet ou partiel, du client, ce dernier devra régler une quote-part de 20% des sommes correspondant aux prestations non encore réalisées.</li>
    </ul>
</div>

<div class="invoice--mentions-block">
    <p><strong>En conformité de l’article L 441-6 du Code de commerce :</strong></p>
    <ul>
        <li>La facture correspondante sera payable sous 60 jours.</li>
        <li>Pas d’escompte pour paiement anticipé.</li>
        <li>Tout règlement effectué après expiration de ce délai donnera lieu, à titre de pénalité de retard, à l’application d’un intérêt égal à celui appliqué par la Banque Centrale Européenne à son opération de refinancement la plus récente, majoré de 10 points de pourcentage, ainsi qu'à une indemnité forfaitaire pour frais de recouvrement d'un montant de 40 Euros.</li>
        <li>Les pénalités de retard sont exigibles sans qu’un rappel soit nécessaire.</li>
    </ul>
</div>

<div class="invoice--mentions-block">
    <p><strong>Informations concernant les droits d'exploitation :</strong></p>
    <ul>
        <li>Le Client s’engage vis à vis du prestataire à disposer de tous les droits d’exploitation nécessaires à la réalisation des prestations objet du présent contrat.</li>
        <li>Le prestataire ne cède que les droits d’exploitation de la création limités aux termes du présent document.</li>
        <li>Le prestataire reste propriétaire de l’intégralité des créations tant que la prestation n’est pas entièrement réglée.</li>
        <li>Toute utilisation sortant du cadre initialement prévu dans ce devis est interdite; sauf autorisation expresse et écrite du prestataire.</li>
    </ul>
</div>
<?php } ?>