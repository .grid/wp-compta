<?php
    // Get actives invoices
    
    $argsSentInvoices = array(
        'post_type' => 'quote',
        'meta_key' => 'invoice_status',
        'meta_value' => 'sent',
        'numberposts' => -1
    );
    $lateInvoices = get_posts( $argsSentInvoices );
    $totalSentInvoices = count($lateInvoices);

    $argsLateInvoices = array(
        'post_type' => 'quote',
        'meta_key' => 'invoice_status',
        'meta_value' => 'late',
        'numberposts' => -1
    );
    $lateInvoices = get_posts( $argsLateInvoices );
    $totalLateInvoices = count($lateInvoices);

    $totalInvoices = $totalSentInvoices + $totalLateInvoices;

    ?>
    <div class="widget invoices-actives">
        <h3>Devis actifs (<?php echo $totalInvoices; ?>)</h3>
        <ul class="invoices-actives--list">
            <li class="invoices-actives--list-sent">Devis envoyés (<span><?php echo $totalSentInvoices; ?></span>)</li>
            <li class="invoices-actives--list-late">Devis périmés (<span><?php echo $totalLateInvoices; ?></span>)</li>
        </ul>
    </div>