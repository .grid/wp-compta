<?php

// INVOICE DETAILS
$invoiceNo = get_the_title();
$invoiceDate = get_the_date('d/m/Y');
$invoiceDescription = get_field('invoice_description');
$currency = '&euro;';
$invoiceTotalPrice = 0;
$type = get_post_type( get_the_ID() );
$deadline = get_field('invoice_deadline');
if ( empty($deadline) ){
    $deadlineValue = 'sous 60 jours';
} else if ( $deadline['value'] === 'on-receipt' ) {
    $deadlineValue = $deadline['label'];
} else {
    $deadlineValue = 'sous ' . $deadline['label'];
}
$invoiceMention = $type === 'quote' ? 'Ce devis est valable 30 jours' : 'Cette facture est payable ' . $deadlineValue;
$invoiceStatus = $type === 'quote' ? 'Devis' : 'Facture';

// COMPANY DETAILS
$companyLogo = '';
$companyName = 'GRID';
$companyAddress = '210 chemin des vergers';
$companyZipcode = '83520';
$companyCity = 'Roquebrune-sur-Argens';
$companyCountry = 'France';
$companyPhone = '+33 7 66 44 92 93';
$companyWebsite = 'www.grid-agency.com';

$companySiren = '818 025 637 00021';
$companyTVA = 'FR09818025637';

$companyIBAN = 'FR76 1679 8000 0100 0019 5687 048';
$companyBIC = 'TRZOFR21XXX';

// CLIENT DETAILS
$clientID = get_field('invoice_client_id');
$clientName = get_the_title($clientID);
$clientAddress = get_field('client_adress', $clientID);
$clientZipcode = get_field('client_zipcode', $clientID);
$clientCity = get_field('client_city', $clientID);
$clientCountry = get_field('client_country', $clientID);

?>
<div class="wrapper">

<div class="invoice--wrap">

<div class="invoice--id">
    <strong><?php echo $invoiceStatus; ?></strong> n° <?php echo $invoiceNo; ?>
</div>

<div class="invoice--head">

    <div class="company-informations">
        <div class="company-id">
            <div class="company-logo">
                <img src="<?php echo untrailingslashit(TEMPLATEPATH); ?>/images/logo-grid.png" alt="GRID">
            </div>
            <p class="company-name"><?php echo $companyName; ?></p>
            <p class="company-address"><?php echo $companyAddress; ?></p>
            <p class="company-zipcode"><?php echo $companyZipcode; ?></p>
            <p class="company-city"><?php echo $companyCity; ?></p>
            <p class="company-country"><?php echo $companyCountry; ?></p>
            <p class="company-phone"><?php echo $companyPhone; ?></p>
            <p class="company-website"><?php echo $companyWebsite; ?></p>
        </div>
        <div class="company-financial-details">
            <p><span>SIREN</span> <?php echo $companySiren; ?></p>
            <p><span>N° TVA</span> <?php echo $companyTVA; ?></p>
        </div>
        <div class="company-bank-details">
            <p><span>IBAN</span> <?php echo $companyIBAN; ?></p>
            <p><span>BIC</span> <?php echo $companyBIC; ?></p>
        </div>
    </div>

    <div class="client-informations">
        <span class="client-label">le client</span>
        <p class="client-name"><?php echo $clientName; ?></p>
        <p class="client-address"><?php echo $clientAddress; ?></p>
        <p class="client-zipcode"><?php echo $clientZipcode; ?></p>
        <p class="client-city"><?php echo $clientCity; ?></p>
        <p class="client-country"><?php echo $clientCountry; ?></p>
    </div>

</div>

<div class="invoice--date">
    Fait le <?php echo $invoiceDate; ?>
</div>

<?php if( !empty($invoiceDescription) ){ ?>
<div class="invoice--description">
    <p><strong>Description</strong></p>
    <?php echo $invoiceDescription; ?>
</div>
<?php } ?>

<div class="invoice--details">
    <?php if( have_rows('invoice_services') ){ ?>
    <table class="invoice--table">
        <thead>
            <tr class="invoice--table-head">
                <th class="invoice--table-head-col description">
                    Détails
                </th>
                <th class="invoice--table-head-col quantity">
                    Qté
                </th>
                <th class="invoice--table-head-col unit-price">
                    Prix unitaire
                </th>
                <th class="invoice--table-head-col tax">
                    TVA %
                </th>
                <th class="invoice--table-head-col total-price">
                    Total
                </th>
            </tr>
        </thead>
        <tbody>
            <?php while( have_rows('invoice_services') ): the_row();
                $service = get_sub_field('invoice_service_group');
                $serviceTitle = $service['invoice_service_title'];
                $descriptionExtras = $service['invoice_service_details'];
                $quantity = floatval(get_sub_field('invoice_service_quantity')) > 0 ? floatval(get_sub_field('invoice_service_quantity')) : '-';
                $tax = '20%';
                $priceUnit = intval(get_sub_field('invoice_service_unit_price'));
                $priceTotal = isset($quantity) && $quantity > 0 ? $quantity * $priceUnit : $priceUnit;

                $invoiceTotalPrice += floatval($priceTotal);
            ?>
            <tr class="invoice--table-row">
                <td class="invoice--table-row-col description">
                    <?php echo $serviceTitle; ?>
                    <?php if( !empty($descriptionExtras) && count($descriptionExtras) > 0 ){ ?>
                    <ul class="description-extras">
                        <?php foreach( $descriptionExtras as $descriptionExtrasRow ) { ?>
                        <li><?php echo $descriptionExtrasRow['invoice_service_detail_list']; ?></li>
                        <?php } ?>
                    </ul>
                    <?php } ?>
                </td>
                <td class="invoice--table-row-col quantity">
                    <?php echo $quantity; ?>
                </td>
                <td class="invoice--table-row-col unit-price">
                    <?php echo number_format($priceUnit, 2, '.', ' ') . $currency; ?>
                </td>
                <td class="invoice--table-row-col tax">
                    <?php echo $tax; ?>
                </td>
                <td class="invoice--table-row-col total-price">
                    <?php echo number_format($priceTotal, 2, '.', ' ') . $currency; ?>
                </td>
            </tr>
            <?php endwhile; ?>
            
            <tr class="invoice--table-subtotal-row">
                <td colspan="4" class="invoice--table-subtotal-col label">
                    Sous-Total HT
                </td>
                <td class="invoice--table-subtotal-col amount">
                    <?php echo number_format($invoiceTotalPrice, 2, '.', ' ') . $currency; ?>
                </td>
            </tr>
            <tr class="invoice--table-subtotal-tax-row">
                <td colspan="4" class="invoice--table-subtotal-tax-col label">
                    TVA
                </td>
                <td class="invoice--table-subtotal-tax-col amount">
                    <?php echo number_format(get_tax_value($invoiceTotalPrice), 2, '.', ' ') . $currency; ?>
                </td>
            </tr>
            <tr class="invoice--table-total-row">
                <td colspan="4" class="invoice--table-total-col label">
                    Total TTC
                </td>
                <td class="invoice--table-subtotal-col amount">
                    <?php
                        $invoiceTotalwTax = floatval($invoiceTotalPrice) + get_tax_value($invoiceTotalPrice);
                        echo number_format($invoiceTotalwTax, 2, '.', ' ') . $currency;
                    ?>
                </td>
            </tr>
        </tbody>
    </table>
    <?php } ?>
</div>
        
<div class="invoice--details-payment">
    <p><?php echo $invoiceMention; ?></p>
</div>

<div class="invoice--mentions">
    <?php
        set_query_var( 'deadline', $deadlineValue );
        get_template_part('template-parts/' . $type . '/mentions');
    ?>
</div>

<div class="invoice--footer">
    <strong>GRID</strong> - EURL - Capital de 5 000&euro; - RCS 818 025 637
</div>

</div><!-- /.invoicewrap -->

</div>