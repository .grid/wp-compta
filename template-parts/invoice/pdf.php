<!DOCTYPE html>

<html <?php language_attributes(); ?>>

	<head>

		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" >

		<title><?php echo get_the_title(); ?></title>
	    <!--<link rel="stylesheet" type="text/css" href="<?php echo untrailingslashit(get_stylesheet_directory_uri()).'/style.css'; ?>" />
	    <link rel="stylesheet" type="text/css" href="<?php echo untrailingslashit(get_stylesheet_directory_uri()).'/print.css'; ?>" />-->
        <style>
            @charset "UTF-8";
            @import url(https://fonts.googleapis.com/css?family=Muli:300,400,600,700&display=swap);
            a,abbr,acronym,address,applet,article,aside,audio,b,big,blockquote,body,canvas,caption,center,cite,code,dd,del,details,dfn,div,dl,dt,em,embed,fieldset,figcaption,figure,footer,form,h1,h2,h3,h4,h5,h6,header,hgroup,html,i,iframe,img,ins,kbd,label,legend,li,mark,menu,nav,object,ol,output,p,pre,q,ruby,s,samp,section,small,span,strike,strong,sub,summary,sup,table,tbody,td,tfoot,th,thead,time,tr,tt,u,ul,var,video{margin:0;padding:0;border:0;font-size:100%;font:inherit;vertical-align:baseline}
            article,aside,details,figcaption,figure,footer,header,hgroup,menu,nav,section{display:block}
            ol,ul{list-style:none}
            blockquote,q{quotes:none}
            blockquote:after,blockquote:before,q:after,q:before{content:'';content:none}
            table{border-collapse:collapse;border-spacing:0}
            .hide-on-phone{display:none}
            .cf:after{display:block;content:'';clear:both}
            .sr-only{position:absolute;width:1px;height:1px;padding:0;margin:-1px;overflow:hidden;clip:rect(0,0,0,0);border:0}
            .sr-only-focusable:active,.sr-only-focusable:focus{position:static;width:auto;height:auto;margin:0;overflow:visible;clip:auto}
            button{cursor:pointer;-webkit-appearance:none;-moz-appearance:none;appearance:none;margin:0;padding:0;background:0 0;border:none}
            button:hover{cursor:pointer}
            .button--blue{display:inline-block;color:#fff;font-size:1.8rem;line-height:1.8rem;font-weight:700;min-width:175px;padding:15px 25px;text-align:center;border:2px solid #00f;border-radius:14px;background-color:#00f}
            img{max-width:100%;height:auto}
            strong{font-weight:700}
            em,i{font-style:italic}
            sup{font-size:.75em;vertical-align:super}
            sub{font-size:.75em;vertical-align:sub}
            a{color:inherit;text-decoration:none}
            .invoice--table{width:100%;margin:20px 0}
            .invoice--table-head{background-color:#f1f1f1}
            .invoice--table-head-col{font-size:1.2rem;line-height:1.4rem;font-weight:600;white-space:nowrap}
            .invoice--table-head-col.description{text-align:left}
            .invoice--table-head-col.quantity{width:7.5%}
            .invoice--table-head-col.unit-price{width:12.5%}
            .invoice--table-head-col.tax{width:7.5%}
            .invoice--table-head-col.total-price{width:15%}
            .invoice--table-row{border-bottom:1px solid #f1f1f1}
            .invoice--table-row:last-of-type{border-bottom:0}
            .invoice--table-row-col.description{font-size:1.5rem;line-height:1.8rem;font-weight:700;text-align:left}
            .invoice--table-row-col.quantity{width:7.5%;text-align:center}
            .invoice--table-row-col.unit-price{width:12.5%;text-align:center}
            .invoice--table-row-col.tax{width:7.5%;text-align:center}
            .invoice--table-row-col.total-price{width:15%;text-align:center}
            td,th{padding:10px}
            .description-extras{font-size:1.3rem;line-height:1.5rem;font-weight:400;margin:5px 0}
            .description-extras li::before{content:'•';margin-right:4px}
            .invoice--table-subtotal-row .label{font-size:1.3rem;line-height:1.5rem;text-align:right}
            .invoice--table-subtotal-row .amount{font-size:1.3rem;line-height:1.5rem;text-align:center}
            .invoice--table-subtotal-tax-row .label{font-size:1.3rem;line-height:1.5rem;text-align:right}
            .invoice--table-subtotal-tax-row .amount{font-size:1.3rem;line-height:1.5rem;text-align:center}
            .invoice--table-total-row{background-color:#f1f1f1}
            .invoice--table-total-row .label{font-size:1.5rem;line-height:1.8rem;font-weight:700;text-align:right}
            .invoice--table-total-row .amount{font-size:1.5rem;line-height:1.8rem;font-weight:700;text-align:center;white-space:nowrap}
            .invoice--id{position:absolute;right:30px;font-size:1.4rem;line-height:1.8rem;text-align:right}
            .invoice--head{position:relative;display:-webkit-box;display:-moz-box;display:-ms-flexbox;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;-moz-box-orient:horizontal;-moz-box-direction:normal;-ms-flex-direction:row;flex-direction:row;margin:20px 0 60px;padding:70px 0 0}
            .company-logo{position:absolute;top:0;max-height:60px}
            .company-logo img{display:block}
            .company-informations{width:60%;font-size:1.2rem;line-height:1.4rem}
            .company-informations div{margin:0 0 10px}
            .company-informations p span{display:inline-block;color:#848484;font-size:1rem;line-height:1.2rem;text-transform:uppercase}
            .client-name,.company-id .client-name,.company-id .company-name,.company-name{font-weight:700;font-size:1.6rem;line-height:2rem;margin-bottom:4px}
            .client-city,.client-zipcode,.company-city,.company-zipcode{display:inline-block}
            .client-informations{width:40%}
            .client-label{display:inline-block;color:#848484;font-size:1rem;line-height:1.2rem;text-transform:uppercase;margin-bottom:5px;text-transform:none}
            *{-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;-webkit-text-size-adjust:100%;-moz-box-sizing:border-box;box-sizing:border-box}
            body,html{position:relative;width:100%;min-height:100%}
            html{font-size:62.5%}
            body{font-family:Muli,sans-serif;font-size:1.4rem;line-height:1.8rem}
            .wrapper{display:-webkit-box;display:-moz-box;display:-ms-flexbox;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;-moz-box-orient:vertical;-moz-box-direction:normal;-ms-flex-direction:column;flex-direction:column;-webkit-box-align:center;-moz-box-align:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-moz-box-pack:center;-ms-flex-pack:center;justify-content:center;min-height:100vh;padding:5vh 5vw;background-color:#f1f1f1}
            .invoice--wrap{position:relative;max-width:900px;padding:30px;box-shadow:0 10px 6px -6px #777;background-color:#fff}
            .invoice--description{margin:20px 0}
            .invoice--mentions{margin:20px 0 0}
            .invoice--mentions p{margin:0 0 8px}
            .invoice--mentions ul li{list-style:disc;margin:0 0 3px 20px}
            .invoice--mentions-block{font-size:1.2rem;line-height:1.4rem;margin-bottom:15px}
            .invoice--mentions-block:last-of-type{margin-bottom:0}
            .invoice--footer{color:#848484;font-size:1.2rem;line-height:1.4rem;margin-top:40px}
            .tag-status{display:inline-block;padding:3px 8px;border-radius:5px}
            .tag-status.draft{color:#926845;background-color:#fdfdea;border:1px solid #ffeeba}
            .tag-status.sent{color:#004085;background-color:#cce5ff;border:1px solid #b8daff}
            .tag-status.paid{color:#155724;background-color:#d4edda;border:1px solid #c3e6cb}
            .tag-status.late{color:#721c24;background-color:#f8d7da;border:1px solid #f5c6cb}
            .invoices--list{margin:40px auto}
            .invoices--list a{color:#00f;text-decoration:underline}
            .invoices--list .price,.invoices--list .total{font-weight:700;text-align:right}
            .invoices--list tfoot{background-color:#f1f1f1}
            @media (min-width:1024px){
            .hide-on-phone{display:block}
            .hide-on-desktop{display:none}
            }
        </style>
	</head>

	<body>

    <?php get_template_part('template-parts/invoice/single-invoice'); ?>

    </body>
</html>