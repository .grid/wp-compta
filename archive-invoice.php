<?php 
get_header();

    $objects = get_queried_object();
    $postType = $objects->name;
    //var_dump($postType);

    /**
     * Calculte Keynumbers
     */
    $totalInvoices = 0;

    // Display all Quotes
    $argsQuotes = array(
        'post_type' => $postType,
        'posts_per_page' => -1
    );

    $queryQuotes = new WP_Query( $argsQuotes );
    ?>
    <div class="wrapper-list">
        <div class="main">
        <?php
        if( $queryQuotes->have_posts() ) :
            ?>
            
                <table id="table-invoices" class="invoices--list">
                    <thead>
                        <tr>
                            <th>Devis</th>
                            <th>Client</th>
                            <th>Montant (&euro; HT)</th>
                            <th>Statut</th>
                            <th>Date d'envoi</th>
                            <th>Date de paiement</th>
                        </tr>
                    </thead>
                <?php
                while( $queryQuotes->have_posts() ) : $queryQuotes->the_post();

                    /**
                     * Init data
                     */
                    $id = get_the_ID();
                    $name = get_the_title();
                    
                    $clientID = get_field('invoice_client_id', $id);
                    $clientName = get_the_title($clientID);

                    $amount = get_field('invoice_amount', $id);
                    $status = wpc_get_status($id);
                    
                    $dateSent = !empty( get_field('invoice_sent_date', $id) ) ? get_field('invoice_sent_date', $id) : '-';
                    $datePaid = !empty( get_field('invoice_paid_date', $id) ) ? get_field('invoice_paid_date', $id) : '-';

                    //$isLate =  ? wpc_invoice_is_late($id) : '';
                    /*if( wpc_invoice_is_late($id) ){
                        $status = 
                    }*/
                    
                    $totalInvoices += floatval($amount);

                ?>
                <tr>
                <?php
                    echo '<td class="invoice-ref"><a href="'. get_permalink() . '">' . get_the_title() . '</a></td>';
                    echo '<td class="client-name">' . $clientName . '</td>';
                    echo '<td class="price">' . number_format($amount, 2, '.', ' ') . '</td>';
                    echo '<td class="status">' . wpc_get_status_tag($status) . '</td>';
                    echo '<td class="date sent">' . $dateSent . '</td>';
                    echo '<td class="date sent">' . $datePaid . '</td>';
                ?>
                </tr>
                <?php
                endwhile;
                wp_reset_postdata();
                ?>
                <tfoot>
                    <tr>
                        <td colspan="2">Total HT</td>
                        <td class="total"><?php echo number_format($totalInvoices, 2, '.', ' '); ?></td>
                        <td colspan="3"></td>
                    </tr>
                </tfoot>
                </table>
            <?php
        else:

        endif;
    ?>
    </div>
    <div class="sidebar">
        <?php get_template_part('template-parts/widgets/invoices-actives'); ?>
        <?php get_template_part('template-parts/widgets/quotes-actives'); ?>
    </div>
</div>
<?php get_footer();